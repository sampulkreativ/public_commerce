module TimeHelper
  def date_only(data)
    data.strftime("%A, %-d %b %Y")
  end

  def datetime_only(data)
    data.strftime("%A, %-d %b %Y %I:%M %p")
  end
end