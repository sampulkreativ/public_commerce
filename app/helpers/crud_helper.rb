module CrudHelper
  def helper_index(object_class, object_key)
    if params[:search].present?
      @pagy, @objects = pagy(object_class.where("#{object_key} ilike ?", "%#{params[:search]}%").order(id: :desc), items: 10)
    else
      @pagy, @objects = pagy(object_class.all.order(id: :desc), items: 10)
    end
  end

  def helper_new(object_class)
    @object = object_class.new
  end

  def helper_create(object_class, object_params, object_message, object_url)
    @object = object_class.new(object_params)
    if @object.save
      flash[:success] = "#{object_message} successfully created"
      redirect_to object_url
    else
      flash[:danger] = "Something went wrong"
      render :new
    end
  end

  def helper_update(object_params, object_message, object_url)
    if @object.update(object_params)
      flash[:success] = "#{object_message} successfully updated"
      redirect_to object_url
    else
      flash[:danger] = "Something went wrong"
      render :edit
    end
  end

  def helper_destroy(object_message, object_url, type=nil)
    @object.destroy
    flash[:success] = "#{object_message} successfully deleted"
    if type == "destroy_user"
      PubnubDoctorRelation.check_user_exist(nil)
      PubnubChatGroupRelation.check_user_exist(nil)
    end
    redirect_to object_url
  end

  def find_object(object_class)
    @object = object_class.find(params[:id])
  end
end