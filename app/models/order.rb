class Order < ApplicationRecord
	belongs_to :user
	has_many :order_variants, dependent: :destroy	

	scope :get_orders, -> (current_user=nil) { where("(user_id = ?)", "#{current_user.try(:id)}").order(created_at: :desc) }
end

# == Schema Information
#
# Table name: orders
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
