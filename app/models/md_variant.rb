class MdVariant < ApplicationRecord
	belongs_to :md_product
	has_many :order_variants, dependent: :destroy
	has_many :users, through: :orders
end

# == Schema Information
#
# Table name: md_variants
#
#  id            :bigint           not null, primary key
#  currency      :string
#  description   :text
#  name          :string
#  price         :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  md_product_id :integer
#
