class OrderVariant < ApplicationRecord
	belongs_to :md_variant
	belongs_to :order
end

# == Schema Information
#
# Table name: order_variants
#
#  id            :bigint           not null, primary key
#  qty           :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  md_variant_id :integer
#  order_id      :integer
#
