class MdStore < ApplicationRecord
	has_many :md_products, dependent: :destroy
	validates_uniqueness_of :name

	scope :get_stores, -> (search=nil) { where("(LOWER(name) ilike ?)", "%#{search.try(:downcase)}%").order(name: :asc) }
end

# == Schema Information
#
# Table name: md_stores
#
#  id          :bigint           not null, primary key
#  address     :text
#  description :text
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
