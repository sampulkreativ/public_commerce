class Device < ApplicationRecord
	include Generateable
	belongs_to :user
  
  validates_uniqueness_of :auth_key, case_sensitive: false, allow_nil: true
  before_create -> { generate_uuid("auth_key") }

  def set_active
    self.status = "active"
  end

  def scope
    self.id.to_s.split("-").join("").to_sym
  end
end

# == Schema Information
#
# Table name: devices
#
#  id         :bigint           not null, primary key
#  auth_key   :string
#  status     :string
#  token      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
