class MdProduct < ApplicationRecord
	belongs_to :md_store
	has_many :md_variants, dependent: :destroy
	validates_uniqueness_of :name

	scope :get_products, -> (search=nil) { where("(LOWER(name) ilike ?)", "%#{search.try(:downcase)}%").order(name: :asc) }
end

# == Schema Information
#
# Table name: md_products
#
#  id          :bigint           not null, primary key
#  description :text
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  md_store_id :integer
#
