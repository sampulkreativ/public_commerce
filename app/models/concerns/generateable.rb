module Generateable
  extend ActiveSupport::Concern

  def generate_uuid(column_name)
    self.send(:"#{column_name}=", SecureRandom.uuid)
  end
end
