class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :name, :email, presence: true
  validates_uniqueness_of :email

  has_many :device, dependent: :destroy
  has_many :orders
  has_many :md_variants, through: :orders

  def generate_jwt
    JWT.encode({id: id,                
                iat: Time.current.to_i, 
                exp: (30).days.from_now.to_i },
                ENV['SECRET_KEY_BASE'])
  end

  def payload(user)        
    token = user.generate_jwt
    device = Device.find_or_create_by(user_id: user.id)        
    payload = {
      auth_token: token,
      user: UserSerializer.new(self, scope: self, scope_name: "current_user").serializable_hash
    }                    
    device.update(token: token, status: "active")        
    return payload
  end

  def current_user_cart
    "cart#{id}"
  end

  def add_to_cart(variant_id)
    $redis.hincrby current_user_cart, variant_id, 1
  end

  def remove_from_cart(variant_id)
    $redis.hdel current_user_cart, variant_id
  end

  def cart_count
    qty = $redis.hvals "cart#{id}"
    qty.reduce(0) {|sum, qty| sum + qty.to_i}
  end

  def cart_total_price
    get_cart_product_variants_with_qty.map { |variant, qty| variant.price * qty.to_i }.reduce(:+)
  end

  def get_cart_product_variants
    cart_ids = []  
    ($redis.hgetall current_user_cart).map do |key|
      cart_ids << key      
    end

    cart_product_variants = MdVariant.find(cart_ids)    
  end

  def get_cart_product_variants_with_qty
    cart_ids = []
    cart_qtys = []
    ($redis.hgetall current_user_cart).map do |key, value|
      cart_ids << key
      cart_qtys << value
    end

    cart_product_variants = MdVariant.find(cart_ids)
    cart_product_variants.zip(cart_qtys)
  end

  def purcase_cart_product_variants!
    order = Order.create(user_id: self.id)
    get_cart_product_variants_with_qty.each do |variant, qty|
      order.order_variants.create(md_variant_id: variant.id, qty: qty)
    end
    $redis.del current_user_cart
  end

end

# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  name                   :string
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#
