class MdStoreSerializer < ActiveModel::Serializer
  attributes :id, :name, :description
  has_many :md_products
end

# == Schema Information
#
# Table name: md_stores
#
#  id          :bigint           not null, primary key
#  address     :text
#  description :text
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
