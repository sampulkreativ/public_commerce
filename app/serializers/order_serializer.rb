class OrderSerializer < ActiveModel::Serializer
  attributes :id, :order_count, :order_total_price
  has_many :order_variants

  def order_count  	
  	object.order_variants.count  	
  end

  def order_total_price
  	object.order_variants.map{|x| x.md_variant.price}.sum
  end
end

# == Schema Information
#
# Table name: orders
#
#  id         :bigint           not null, primary key
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
