class MdProductSerializer < ActiveModel::Serializer
  attributes :id, :name, :description
  belongs_to :md_store
  has_many :md_variants
end

# == Schema Information
#
# Table name: md_products
#
#  id          :bigint           not null, primary key
#  description :text
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  md_store_id :integer
#
