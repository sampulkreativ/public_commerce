class OrderVariantSerializer < ActiveModel::Serializer
  attributes :id, :md_variant
  

  def md_variant  	
  	ActiveModelSerializers::SerializableResource.new(object.md_variant, serializer: MdVariantSerializer)    
  end
end

# == Schema Information
#
# Table name: order_variants
#
#  id            :bigint           not null, primary key
#  qty           :integer
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  md_variant_id :integer
#  order_id      :integer
#
