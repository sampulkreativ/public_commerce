class MdVariantSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :currency, :price
  belongs_to :md_product
end

# == Schema Information
#
# Table name: md_variants
#
#  id            :bigint           not null, primary key
#  currency      :string
#  description   :text
#  name          :string
#  price         :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  md_product_id :integer
#
