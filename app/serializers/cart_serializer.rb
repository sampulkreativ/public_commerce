class CartSerializer < ActiveModel::Serializer
  attributes :id, :name, :currency,:price, :description
  belongs_to :md_product

end
