class AdminController < ActionController::Base
  include Pagy::Backend
  include CrudHelper
  before_action :authenticate_admin!
  layout "admin"
end
