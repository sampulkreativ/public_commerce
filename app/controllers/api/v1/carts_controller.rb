class Api::V1::CartsController < ApiController
	before_action :authenticate_request!

	def show
		carts = @current_user.get_cart_product_variants		
		paginate_carts = paginate carts, per_page: params[:per_page], page: params[:page], count: carts.count   		
    api(each_serializer(paginate_carts, CartSerializer))
	end

	def add
		@current_user.add_to_cart(params[:variant_id])
		api({message: I18n.t("controller.render_json.success")})
	end

	def remove
		@current_user.remove_from_cart(params[:variant_id])
		api({message: I18n.t("controller.render_json.success")})
	end	
end
