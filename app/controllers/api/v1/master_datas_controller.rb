class Api::V1::MasterDatasController < ApiController
	def stores
		begin
	    stores = MdStore.get_stores(params[:search])
	    paginate_stores = paginate stores, per_page: params[:per_page], page: params[:page], count: stores.count
      api(each_serializer(paginate_stores, MdStoreSerializer))
		rescue
	    render_json(404, I18n.t("controller.error_render_json.page_not_found"))
	  end
	end

	def products
		begin			
			products = MdProduct.get_products(params[:search])
	    paginate_products = paginate products, per_page: params[:per_page], page: params[:page], count: products.count
      api(each_serializer(paginate_products, MdProductSerializer))
	  rescue
	    render_json(404, I18n.t("controller.error_render_json.page_not_found"))
	  end
	end
end
