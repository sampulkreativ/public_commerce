class Api::V1::RegistrationsController < ApiController		
	def create
    begin            
      user = User.new(user_params)
      if user.save        
       	message = I18n.t("controller.render_json.register_success")
        api({message: message})        
      else
        render_json(422, user.errors.full_messages, { data: user })
      end
    rescue ActiveRecord::RecordNotFound => e
      render json: { errors: e.message }, status: :unauthorized    
    end
  end  

  private
  def user_params
    params.require(:user).permit(:name, :email, :password)
  end
end
