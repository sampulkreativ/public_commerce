class Api::V1::TransactionsController < ApiController
	before_action :authenticate_request!
	before_action :check_cart!

	def create
		#it should be available for payment gatway transaction callback
		# if payment_gateway_callback == success
			@current_user.purcase_cart_product_variants!
			message = I18n.t("controller.render_json.transaction.success")
      api({message: message})   
		# else
		# end
	end

	private
	def check_cart!
		if @current_user.get_cart_product_variants_with_qty.blank?			
      render json: { errors: I18n.t("controller.error_render_json.cart_empty") }, status: 404
		end
	end
end
