class Api::V1::OrdersController < ApiController
	before_action :authenticate_request!

	def index
		#begin
	    orders = Order.get_orders(@current_user)
	    paginate_orders = paginate orders, per_page: params[:per_page], page: params[:page], count: orders.count
      api(each_serializer(paginate_orders, OrderSerializer))
		# rescue
	 #    render_json(404, I18n.t("controller.error_render_json.page_not_found"))
	 #  end
	end
end
