class Api::V1::SessionsController < Devise::SessionsController
	include StatusCode
	include ResponseApi	
  skip_forgery_protection

	def create
		user = User.find_by_email(params[:email])

    if user && user.valid_password?(params[:password])
      @current_user = user
      api user.payload(user)
    else    	    	
      response = {
        status: I18n.t("controller.error_render_json.error"),
        message: I18n.t("controller.error_render_json.sign_in_invalid")
      }      
      render json: response, status: 404      
    end
	end	
end
