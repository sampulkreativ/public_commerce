class Admins::UsersController < AdminController
  before_action only: [:show, :edit, :update, :destroy] do find_object(User) end

  def index
    helper_index(User, "email")
  end

  def new
    helper_new(User)
  end

  def create
    helper_create(User, object_params, "User", admins_users_url)
  end

  def edit
  end

  def update
    helper_update(object_params, "User", admins_users_url)
  end

  def destroy
    helper_destroy("User", admins_users_url)
  end

  private
  def object_params
    begin
      params.require(:user).permit(:name, :email, :password)
    rescue ActionController::ParameterMissing => e
      return {}
    end
  end
end