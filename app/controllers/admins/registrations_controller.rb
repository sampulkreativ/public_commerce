class Admins::RegistrationsController < AdminController
  def edit
    @object = current_admin
  end

  def update
    @object = current_admin

    if @object.update(object_params)
      flash[:success] = "Admin successfully updated"
      redirect_to admins_edit_url
    else
      flash[:danger] = "Something went wrong"
      render :edit
    end
  end

  private
  def object_params
    begin
      params.require(:admin).permit(:name, :email, :password)
    rescue ActionController::ParameterMissing => e
      return {}
    end
  end
end