class Admins::DashboardsController < AdminController
  def index
    @user_today = User.counting(Time.now.beginning_of_day, Time.zone.now.end_of_day)
    @user_week = User.where(created_at: Time.now.beginning_of_week(:monday)).count
    @user_month = User.counting(Time.now.beginning_of_month, Time.zone.now.end_of_month)

    if params[:email] || params[:name]
      @pagy, @users = pagy(User.where("email ilike ?", "%#{params[:email]}%").where("name ilike ?", "%#{params[:name]}%").order(id: :desc), items: 10)
    else
      @pagy, @users = pagy(User.all.order(id: :desc), items: 10)
    end
  end
end