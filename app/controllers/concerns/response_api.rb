module ResponseApi
  extend ActiveSupport::Concern

  included do
  end
  
  private

    def api data
      render json: {
        response: {
          status: status,
          message: http_status_codes(status),
          url: request.original_url
        },
        data: data.nil? ? nil : (JSON.parse(data) rescue data)
      }
    end

    def api_custom data, status, message
      if message.present?
        render json: {
          response: {
            status: status,
            message: message,
            url: request.original_url
          },
          data: data.nil? ? nil : (JSON.parse(data) rescue data)
        }
      else
        render json: {
          response: {
            status: status,
            url: request.original_url
          },
          data: data.nil? ? nil : (JSON.parse(data) rescue data)
        }
      end
    end

    def each_serializer resource, serializer, options = {}
      ActiveModelSerializers::SerializableResource.new(resource, each_serializer: serializer, options: options)
    end

    def serializer resource, serializer, options = {}
      ActiveModelSerializers::SerializableResource.new(resource, serializer: serializer, options: options)
    end

    def user
      Example::Namespace::User.find_by(id: payload["user_id"])
    rescue
      nil
    end

    def user_id
      user.nil? ? nil : user.id
    end
end
