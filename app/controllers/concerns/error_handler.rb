module ErrorHandler
  require 'httparty'
  require 'pp'

  include StatusCode

  extend ActiveSupport::Concern

  included do
    rescue_from ActionController::RoutingError do |error|
      logger.debug error
      #error_notification(error, 404)
      slack_error_notification(error, 404)
    end

    rescue_from StandardError do |error|
      logger.debug error
      #error_notification(error, 500)
      slack_error_notification(error, 500)
    end
  end

  private
  def api_client(url, data = nil, method = "get", header = nil)
    if method == "get"
      if data.nil?
        JSON.parse(RestClient.get url, header)
      else
        JSON.parse(RestClient.get url, data, header)
      end
    elsif method == "post"
      JSON.parse(RestClient.post url, data, header)
    end
  rescue => exception
    exception.response
  end

  def userstack
    api_client("http://api.userstack.com/detect?access_key=1dea8ffa3208961facecbcec003ebf73&ua=#{request.user_agent}")
  rescue
    nil
  end

  def ip_lookup
    begin
      api_client("http://api.ipapi.com/#{request.remote_ip}?access_key=1a35a0c6e2ba5d8ef26607b49bafd063")
    rescue
      begin
        api_client("http://api.ipstack.com/#{request.remote_ip}?access_key=1a35a0c6e2ba5d8ef26607b49bafd063")
      rescue
        nil
      end
    end
  rescue
    nil
  end

  def error_notification(error, status_code)
    flock_api_url = "https://api.flock.com/hooks/sendMessage/a2d44f57-d09d-48de-b33c-bcb6ee32f0d6"
    unless Rails.env.development?
      ExceptionNotifier.notify_exception(
          error,
          data: {
              class_exception: error.class,
              rails_env: Rails.env,
              message: error.message,
              backtrace: error.backtrace[0]
          }
      )

      @request = ActionDispatch::Request.new(request.env)

      text = <<-EOF
      *Rails Environment:*
      #{Rails.env}

      *HTTP Status Code:*
      #{http_status_codes(status_code)}

      *Error Class:*
      #{error.class}

      *Message:*
      #{error.message}

      *First Backtrace:*
      #{error.backtrace[0]}

      *Request:*
      URL: #{request.original_url}
      HTTP Method: #{request.method}
      IP Address: #{request.remote_ip}
      Parameters: #{JSON.pretty_generate(request.filtered_parameters)}
      Timestamp: #{Time.current}
      Server: #{Socket.gethostname}
      Rails Root: #{Rails.root}
      Process: #{$$}

      *Session:*
      Session ID: #{@request.ssl? ? "[FILTERED]" : (@request.session["session_id"] || (@request.env["rack.session.options"] and @request.env["rack.session.options"][:id])).inspect.html_safe}
      Data: #{@request.session}

      *User Lookup*
      IP Lookup: #{JSON.pretty_generate(ip_lookup)}
      User Stack: #{JSON.pretty_generate(userstack)}
            EOF
      HTTParty.post(flock_api_url,
                    body: {text: text}.to_json,
                    headers: {"Content-Type": "application/json"})
    end
    rescue => exception
      logger.debug exception
  end

  def slack_error_notification(error, status_code)
    slack_api_url = "https://hooks.slack.com/services/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"   
    unless Rails.env.development?      
      ExceptionNotifier.notify_exception(
          error,
          data: {
              class_exception: error.class,
              rails_env: Rails.env,
              message: error.message,
              backtrace: error.backtrace[0]
          }
      )   

      @request = ActionDispatch::Request.new(request.env)

      text = <<-EOF
      *Rails Environment:*
      #{Rails.env}

      *HTTP Status Code:*
      #{http_status_codes(status_code)}

      *Error Class:*
      #{error.class}

      *Message:*
      #{error.message}

      *First Backtrace:*
      #{error.backtrace[0]}

      *Request:*
      URL: #{request.original_url}
      HTTP Method: #{request.method}
      IP Address: #{request.remote_ip}
      Parameters: #{JSON.pretty_generate(request.filtered_parameters)}
      Timestamp: #{Time.current}
      Server: #{Socket.gethostname}
      Rails Root: #{Rails.root}
      Process: #{$$}
            EOF
      HTTParty.post(slack_api_url,
                    body: {text: text}.to_json,
                    headers: {"Content-Type": "application/json"})
    end
  end
end