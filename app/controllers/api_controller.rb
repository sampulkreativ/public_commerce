class ApiController < ActionController::API
	include StatusCode
  include ApplicationHelper
  include ResponseApi
  include ErrorHandler unless Rails.env.development?
  include ActionView::Helpers::NumberHelper
  attr_reader :current_user
  attr_reader :current_device
  
  before_action :set_locale  
  respond_to :json    

  protected
    def authenticate_request!   
      unless user_id_in_token?
        not_authenticated
        return
      end
      @current_device = Device.find_by_token(@http_token)      
      if @current_device.present?        
        @current_user = User.find_by(id: @current_device.user_id)
      else
        token_change
        return
      end

      not_authenticated if @current_user.blank?
    rescue JWT::VerificationError, JWT::DecodeError
      not_authenticated
    end

    def set_locale
      I18n.locale = params[:locale].blank? ? "en" : params[:locale]
    end

    def find_object(var_name, params_name="id", key_name="id")
      begin
        value = var_name.camelize.constantize.find_by("#{key_name}": "#{params[params_name]}")
      rescue ActiveRecord::RecordNotFound
        value = nil
      end
      instance_variable_set "@#{var_name}".to_sym, value
      if instance_variable_get("@#{var_name}").blank?
        render_json(404, "#{var_name.capitalize.humanize} not found")
      else
        return value
      end
    end

    def request_path
      request.path.split("/").last.split("?").first
    end

    def render_json(http_code, message, data=nil)
      status = http_code.to_s.first.to_i.eql?(2) ? "success" : "error"
      response = {
        status: status,
        message: message
      }
      response = response.merge(data) if data
      render json: response, status: http_code
    end

  private
    def http_token            
      @http_token ||= if request.headers['Authorization'].present?
        request.headers['Authorization'].split(' ').last
      end
    end

    def auth_token
      @auth_token ||= JsonWebToken.decode(http_token)
    end

    def user_id_in_token?      
      #http_token && auth_token && auth_token[:user_id].to_i && auth_token[:user_agent]      
      http_token && auth_token && auth_token[:user_id].to_i
    end

    def not_authenticated
      render json: { errors: 'Not Authenticated' }, status: :unauthorized
    end

    def token_change
      render json: { errors: 'API Token has changed' }, status: :unauthorized
    end
end
