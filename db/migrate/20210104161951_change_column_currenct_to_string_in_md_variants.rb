class ChangeColumnCurrenctToStringInMdVariants < ActiveRecord::Migration[6.0]
  def change
  	remove_column :md_variants, :currency
  	add_column :md_variants, :currency, :string, default: "Rp"
  end
end
