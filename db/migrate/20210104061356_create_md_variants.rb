class CreateMdVariants < ActiveRecord::Migration[6.0]
  def change
    create_table :md_variants do |t|
      t.string :name
      t.text :description
      t.float :price
      t.float :currency, default: "Rp"
      t.integer :md_product_id

      t.timestamps
    end
    add_index :md_variants, [:md_product_id]
  end
end
