class RemoveColumnMdVariantIdInOrder < ActiveRecord::Migration[6.0]
  def change
  	remove_column :orders, :md_variant_id
  	remove_column :orders, :qty
  end
end
