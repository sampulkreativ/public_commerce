class CreateDevices < ActiveRecord::Migration[6.0]
  def change
    create_table :devices do |t|
      t.string :auth_key
      t.string :status
      t.string :token
      t.integer :user_id

      t.timestamps
    end
    add_index :devices, [:user_id]
  end
end
