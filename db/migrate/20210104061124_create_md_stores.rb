class CreateMdStores < ActiveRecord::Migration[6.0]
  def change
    create_table :md_stores do |t|
      t.string :name
      t.text :description
      t.text :address

      t.timestamps
    end
  end
end
