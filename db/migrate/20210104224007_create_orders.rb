class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.integer :md_variant_id
      t.integer :user_id
      t.integer :qty

      t.timestamps
    end
    add_index :orders, [:md_variant_id, :user_id]
  end
end
