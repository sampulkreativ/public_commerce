class CreateOrderVariants < ActiveRecord::Migration[6.0]
  def change
    create_table :order_variants do |t|
      t.integer :order_id
      t.integer :md_variant_id
      t.integer :qty

      t.timestamps
    end
  end
end
