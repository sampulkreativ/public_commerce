class CreateMdProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :md_products do |t|
      t.string :name
      t.text :description       
      t.integer :md_store_id

      t.timestamps
    end
    add_index :md_products, [:md_store_id]
  end
end
