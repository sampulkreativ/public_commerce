puts "Creating Products"
ActiveRecord::Base.connection.execute("TRUNCATE md_products RESTART IDENTITY CASCADE")
beginning_time = Time.current

MdStore.all.each do |store|
	SEED_DEFAULT.times do
	  MdProduct.create(
      name: Faker::Vehicle.manufacture,
      description: Faker::Lorem.paragraph,
      md_store_id: store.id
	  )
	end
end
end_time = Time.current
puts "Fake Products Created Successfully in #{(((end_time - beginning_time) * 1000) / 1000).to_s[0..3]} seconds"