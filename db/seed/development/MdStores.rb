puts "Creating Stores"
ActiveRecord::Base.connection.execute("TRUNCATE md_stores RESTART IDENTITY CASCADE")
beginning_time = Time.current

SEED_DEFAULT.times do
  MdStore.create(
      name: Faker::Commerce.product_name,
      description: Faker::Lorem.paragraph
  )
end
end_time = Time.current
puts "Fake Stores Created Successfully in #{(((end_time - beginning_time) * 1000) / 1000).to_s[0..3]} seconds"