puts "Creating Variants"
ActiveRecord::Base.connection.execute("TRUNCATE md_variants RESTART IDENTITY CASCADE")
beginning_time = Time.current

MdProduct.all.each do |product|
	SEED_DEFAULT.times do
	  MdVariant.create(
      name: Faker::Superhero.name,
      description: Faker::Lorem.paragraph,
      price: Faker::Number.number(digits: 6),
      currency: "Rp",
      md_product_id: product.id
	  )
	end
end
end_time = Time.current
puts "Fake Variant Created Successfully in #{(((end_time - beginning_time) * 1000) / 1000).to_s[0..3]} seconds"