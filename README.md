# Project-Initialization

## Introduction

- This application was created for implementing Rest API E-Commerce
- Please prepared API Request tool

## Tech Stack

- You can see on [Gemfile](Gemfile)

## Requirements

- Ruby 2.7.0
- Rails 6.0.3
- PostgreSQL
- Redis server 6.0.5

## Configuration

- Ruby
- PostgreSQL
- Postman

## Setup

- Install your Ruby on Rails Environment (Rbenv or RVM)
- Go to app root folder
- Run ```bundle install```
- Run important command ```rails db:create db:migrate db:seed``` to create database and generate migration also get dummy data
- After finish your installation you can run ```rails server``` to run the application from your computer (localhost)
- Run your redis ```redis-server``` id default port redis://127.0.0.1:6379
- Default port of ```rails server``` is localhost:3000
- Open your postman app and import all of datas which located on postman_data/
- There's a sections for Authorization, Master data and Order

## Sections

- Authorization to auth your account such as: register and login for get auth_token
- Master data to see your dummy data
- Order to create cart and running checkout until you get order list