Rails.application.routes.draw do  
  devise_for :admins, path_names: { sign_in: 'login', sign_out: 'logout' }, controllers: { sessions: 'admins/sessions' }, skip: [:registrations, :passwords]
  namespace :admins do
    get 'edit' => 'registrations#edit'
    post 'update' => 'registrations#update'
    resources :dashboards, only: :index    
    resources :users
  end

  namespace :api do
    namespace :v1 do
      devise_for :users, path_names: { sign_in: 'sign_in', sign_out: 'sign_out' }
      post 'sign_up' => 'registrations#create', as: :sign_up

      #master data
      resources :master_datas, only: [:index] do
        collection do
          get :stores
          get :products
        end
      end

      resources :carts, only: [:show] do
        put 'add/:variant_id', to: "carts#add", as: :add_to
        put 'remove/:variant_id', to: 'carts#remove', as: :remove_from
        put 'remove_one/:variant_id', to: 'carts#removeone', as: :remove_one
      end

      resources :transactions, only: [:new, :create]
      resources :orders, only: [:index]
    end
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
