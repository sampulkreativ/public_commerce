# config valid for current version and patch releases of Capistrano
lock "~> 3.13.0"

set :application, "project-initialization"
set :repo_url, "git@github.com:dhamkur/project-initialization.git"

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '2.6.3'

# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, "/home/production/apps/project-initialization"
set :deploy_to, "/home/staging/apps/project-initialization" # Uncomment this if you want to deploy to staging

set :puma_bind, "tcp://0.0.0.0:3367"
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log, "#{release_path}/log/puma.access.log"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true
set :keep_releases, 5
append :linked_files, "config/database.yml", "config/secret.yml", "config/local_env.yml"
set :linked_dirs, %w{ log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads }