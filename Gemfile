source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

gem 'rails',                          '~> 6.0.2', '>= 6.0.2.1'
gem 'pg',                             '>= 0.18', '< 2.0'
gem 'puma',                           '~> 4.1'
gem 'sass-rails',                     '>= 6'
gem 'webpacker',                      '~> 4.0'
gem 'turbolinks',                     '~> 5'
gem 'jbuilder',                       '~> 2.7'
gem 'devise',                         '~> 4.7', '>= 4.7.1'
gem 'shrine',                         '~> 3.2', '>= 3.2.1'
gem 'whenever',                       '~> 1.0'
gem 'sidekiq',                        '~> 6.0', '>= 6.0.6'
gem 'device_detector',                '~> 1.0', '>= 1.0.3'
gem 'acts_as_paranoid',               '~> 0.6.3'
gem 'pagy',                           '~> 3.7', '>= 3.7.5'
gem 'api-pagination',                 '~> 4.8', '>= 4.8.2'
gem 'aws-sdk-s3',                     '~> 1.61', '>= 1.61.2'
gem 'dotenv-rails',                   '~> 2.7', '>= 2.7.5'
gem 'exception_notification',         '~> 4.4'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap',                       '>= 1.4.2', require: false

# Redis for database memory
gem 'redis'
gem 'redis-namespace'
gem 'redis-rails'

# API
gem 'jwt',                            '2.2.1'
gem 'active_model_serializers',       '0.10.10'
gem 'rest-client',                    '2.1.0'
gem 'oj',                             '3.9.2'
gem 'rack-cors',                      '~> 1.1', '>= 1.1.1'
gem 'rack-cache',                     '~> 1.11', '>= 1.11.1'
gem 'httparty',                       '~> 0.18'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console',                  '>= 3.3.0'
  gem 'listen',                       '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  # gem 'spring'
  # gem 'spring-watcher-listen',        '~> 2.0.0'

  # Better Errors
  gem 'better_errors',                '~> 2.6'
  gem 'binding_of_caller',            '~> 0.8.0'

  # Documenting Tables
  gem 'annotate',                     '~> 3.1', '>= 3.1.1'

  # Faker
  gem 'faker',                        '~> 2.11'

  # Deployment
  gem 'capistrano',                   '>= 3.11.2',            require: false
  gem 'capistrano-rbenv',             '~> 2.1',               require: false
  gem 'capistrano-bundler',           '~> 1.6',               require: false
  gem 'capistrano-rails',             '~> 1.4',               require: false
  gem 'capistrano3-puma',             '~> 4.0',               require: false
  gem 'capistrano-rails-collection',  '~> 0.1.0',             require: false
end

group :test do
  # Adds support for Capybara system testing and selenium driver
  gem 'capybara',                     '>= 2.15'
  gem 'selenium-webdriver'
  # Easy installation and use of web drivers to run system tests with browsers
  gem 'webdrivers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]